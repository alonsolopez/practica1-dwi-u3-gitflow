<?php

use App\Http\Controllers\ConsultarAereopuertosController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//endpoint para calcular el area del triangulo
// areaTriangulo = (b*h)/2
// Route::post('/areaTriangulo/{b}/{h}', function ($b, $h)
// {
//     return ($b * $h) / 2;
// });

//Endpoint para calcular el area de un cuadrado
Route::post('/areacuadradoipolito', function (Request $request) {
    return ($request->l * $request->l);
});


Route::post('/areaTriangulo', function (Request $request) {
    return ($request->b * $request->h) / 2;
});


//Endpoint para calcular el área del Rectangulo

Route::post('/areaRectangulo', function (Request $request) {
    return ($request->b * $request->h);
});



Route::post('/areaCirculo', function (Request $request) {
    return ($request->r * 3.1416 * $request->r);
});
Route::get('/rene_moreno/', function (Request $request) {


    $curl = curl_init();

    curl_setopt_array($curl, [
        CURLOPT_URL => "https://aerodatabox.p.rapidapi.com/airports/search/term?q=schiphol&limit=10",
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => [
            "X-RapidAPI-Host: aerodatabox.p.rapidapi.com",
            "X-RapidAPI-Key: 52f7c00c51msh19a7d5affcbf0bcp1f7800jsn078b3dd71093"
        ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        echo $response;
    }
});

Route::get('consultar/aereopuertosipolito',[ConsultarAereopuertosController::class, 'consultarAeropuertosipolito'])->name('consultarAeropuertosipolito');
