<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConsultarAereopuertosController extends Controller
{
    //llevar acabo consultas consumiento api de aerodata de RapidAPI
    public function consultarAeropuertosipolito(Request $request)
    {
        //dum
        dd($request->q);
        //pregamos codigo de api

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://aerodatabox.p.rapidapi.com/airports/search/term?q=$request->q&limit=10",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "X-RapidAPI-Host: aerodatabox.p.rapidapi.com",
                "X-RapidAPI-Key: 3e734da75amsh6e0062363640646p1eaef6jsnb485ae702f65"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $responseErr = [
                "err" => $err,
                "status" => 500
            ];
            //conrmar response de tipo Json
            //return $responseData;
            return response()->json($responseErr, 500);
        } else {
            //desserializar el json
            $responseJson = json_decode($response);

            if (isset($responseJson->items[0])) {

                $responseData = [
                    "data" => [
                        "icao" => $responseJson->items[0]->icao,
                        "iata" => $responseJson->items[0]->iata,
                        "name" => $responseJson->items[0]->name,
                        "shortName" => $responseJson->items[0]->shortName,
                        "municipalityName" => $responseJson->items[0]->municipalityName,
                        "location" => [
                            "lat" => $responseJson->items[0]->location->lat,
                            "lon" => $responseJson->items[0]->location->lot,
                        ],
                        "countryCode" => $responseJson->items[0]->countryCode
                    ],
                    "status" => 200
                ];
                //conrmar response de tipo Json
                //return $responseData;
                return response()->json($responseData, 200);
            }else
            {
                $responseErr = [
                    "err" => "no hay datos de respuesta sobre tu consulta'".$request->q."'",
                    "status" => 500
                ];
                //conrmar response de tipo Json
                //return $responseData;
                return response()->json($responseErr, 500);

            }
        }
    }
}
