﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace TareaAlonsoPRACTICAS.Controllers
{
    public class AdanController : ApiController
    {

        [HttpPost]
        public dynamic OperacionTriangulo(double _altura, double _base)
        {
            var Area = (_base * _altura) / 2;
            var objResponse = new
            {
                Area = Area,
            };
            return objResponse;
        }
    }
}